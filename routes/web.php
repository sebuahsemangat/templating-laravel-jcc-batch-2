<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Route::get('table', function () 
{
    return view('pages.table');
});

Route::get('data-tables', function () 
{
    return view('pages.data-tables');
});

// Route /cast simpan data dengan metode post
Route::post('cast', 'CastController@store');

// Route /cast menampilkan data dengan metode get
Route::get('cast', 'CastController@index');


//routing untuk tampilan input data
Route::get('cast/create', 'CastController@create');

//Routing untuk show detil berdasarkan id
Route::get('/cast/{id}', 'CastController@show');

//Routing untuk menuju form edit
Route::get('/cast/{id}/edit', 'CastController@edit');

//Routing untuk proses edit data cast
Route::put('/cast/{id}', 'CastController@update');

//Routing untuk delete
Route::delete('/cast/{id}', 'CastController@destroy');