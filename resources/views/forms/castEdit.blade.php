@extends('blank')

@section('judul')
Edit Film Cast
@endsection('judul')

@section('subjudul')
Halaman ini digunakan untuk edit data tabel cast
@endsection('subjudul')

@section('content')
@if(session('success'))
<p class="alert alert-success">
    {{session('success')}}
</p>
@endif
    <form role="form" action="/cast/{{ $castinfo->id }}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label for="nama">Nama Cast</label>
        <input type="text" class="form-control" id="nama" placeholder="Masukkan Nama" name="nama" value="{{old('nama',$castinfo->nama)}}">
        @error('nama')
        <p class="alert alert-danger">{{ $message }}</p>
        @enderror
    </div>
    <div class="form-group">
        <label for="umur">Umur Cast</label>
        <input type="number" class="form-control" id="umur" placeholder="Masukkan Umur" name="umur" value="{{old('umur',$castinfo->umur)}}">
        @error('umur')
        <p class="alert alert-danger">{{ $message }}</pv>
        @enderror
    </div>
    <div class="form-group">
        <label for="bio">Bio Cast</label>
        <textarea class="form-control" id="bio" name="bio">{{old('bio',$castinfo->bio)}}</textarea>
        @error('bio')
        <p class="alert alert-danger">{{ $message }}</p>
        @enderror
    </div>
                  

    <button type="submit" class="btn btn-primary">Update</button>

    </form>
        
@endsection('content')

@section('footer')
Edit Film Cast
@endsection('footer')