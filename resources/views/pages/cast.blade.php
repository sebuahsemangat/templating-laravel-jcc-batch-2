@extends('blank')
@section('judul')
Film Cast
@endsection('judul')
@section('subjudul')
Halaman ini menampilkan data dari tabel cast
@endsection('subjudul')
@push('scripts')
<script src="{{asset('/adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush('scripts')

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css"/>
 
@endpush('style')
@section('content')

@if(session('success'))
<p class="alert alert-success">
    {{session('success')}}
</p>
@endif

<a href="/cast/create" class="btn btn-primary btn-md mb-3"><i class="fa fa-solid fa-plus"></i> Tambah Data</a>


        <table id="example1" class="table">
          <thead>
          <tr>
            <th style="width: 40px;">No.</th>
            <th>Nama</th>
            <th>Umur</th>
            <th>Bio</th>
            <th style="">Action</th>
          </tr>
          </thead>
          <tbody>
            @forelse($cast as $key => $casting)
            <tr>
            <td>{{ $key +1 }}</td>
            <td>{{ $casting->nama }}</td>
            <td>{{ $casting->umur }} Tahun</td>
            <td>{{ Str::limit($casting->bio, 50) }}</td>
            <td>
                
                <form action="/cast/{{ $casting->id }}" method="POST" class="form-inline">
                  @csrf
                  @method('DELETE')
                  <a href="/cast/{{ $casting->id }}" class="btn btn-info btn-xs mr-1"><i class="fa fa-search"></i> Lihat</a>
                  <a href="/cast/{{ $casting->id }}/edit" class="btn btn-warning btn-xs mr-1"><i class="fa fa-pen"></i> Edit</a>
                  <button type="submit" class="btn btn-danger btn-xs">Delete</button>
                </form>
            </td>
            </tr>
            @empty
            <tr><td colspan="4" align="center">Data cast masih kosong</td></tr>
            @endforelse
          
          </tbody>
          <tfoot>
          <tr>
            <th>No.</th>
            <th>Nama</th>
            <th>Umur</th>
            <th>Bio</th>
            <th>Action</th>
          </tr>
          </tfoot>
        </table>

@endsection('content')
@section('footer')
Film Cast
@endsection('footer')