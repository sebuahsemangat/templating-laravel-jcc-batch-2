@extends('blank')
@section('judul')
Film Cast Info
@endsection('judul')
@section('subjudul')
Halaman ini menampilkan detil data dari tabel cast berdasarkan ID-nya
@endsection('subjudul')
@push('scripts')
<script src="{{asset('/adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush('scripts')

@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.5/datatables.min.css"/>
 
@endpush('style')


@section('content')
<h2>
    {{$castinfo->nama}}
</h2>
<p>Umur: {{$castinfo->umur}}</p>
<p>Bio: {{$castinfo->bio}}</p>

<a href="/cast" class="btn btn-md btn-primary"><i class="fa fa-solid fa-arrow-left"> </i> Kembali</a>
@endsection('content')