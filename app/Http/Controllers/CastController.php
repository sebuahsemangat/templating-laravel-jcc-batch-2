<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class CastController extends Controller
{
    public function create() {
        return view ('forms.castCreate');
    }


    public function store(Request $request){
    //dd($request->all());
    //validasi
    $request->validate([
        "nama" => 'required',
        "umur" => 'required',
        "bio" => 'required'
    ]);

    $query = DB::table('cast')->insert([
        "nama" => $request["nama"],
        "umur" => $request["umur"],
        "bio" => $request["bio"]
    ]
    );
    //redirect ke halaman create
    return redirect('/cast/create')->with('success','Data cast berhasil ditambahkan.');
    }


    //ini controller untuk menampilkan data
    public function index(){
        $cast = DB::table('cast')->get(); //sama dengan select * from cast
        return view ('pages.cast', compact('cast'));
    }

    public function show($id) {
        $castinfo = DB::table('cast')->where('id', $id)->first();//first untuk menampilkan satu data
       // dd($castinfo);
        return view ('pages.castShow', compact('castinfo'));
    }

    public function edit($id){
        $castinfo = DB::table('cast')->where('id', $id)->first();
        return view ('forms.castEdit', compact('castinfo'));
    }
    public function update($id, Request $request){
        $request->validate([
            "nama" => 'required',
            "umur" => 'required',
            "bio" => 'required'
        ]);
        $query = DB::table('cast')
                    ->where('id', $id)
                    ->update([
                        'nama' => $request['nama'],
                        'umur' => $request['umur'],
                        'bio' => $request['bio']
                    ]);
        return redirect ('/cast')->with('success', 'Data berhasil diupdate!');
    }
    public function destroy($id, Request $request){
        $query = DB::table('cast')
                ->where('id', $id)
                ->delete();
        return redirect ('/cast')->with('success', 'Data berhasil dihapus!');
    }
}
